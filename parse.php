<?php
include_once('simple_html_dom.php');

/**
 * Created by PhpStorm.
 * User: InnerFlame
 * Date: 08.01.15
 * Time: 15:56
 */
class Parse{

    const SITE_URL = 'http://flags.redpixart.com/rus/list/';
    const SITE_PATH = 'table.country tbody tr';

    /**
     * @var array
     */
    private $full;

    /**
     * @var array
     */
    private $rus;

    /**
     * @var array
     */
    private $eng;

    /**
     * @var array
     */
    private $link;

    /**
     *
     */
    function __construct(){
        $html = file_get_html(self::SITE_URL);
        $nodes = $html->find(self::SITE_PATH);

        foreach($nodes as $element){
            $this->rus[] = strip_tags($element->children(1));
            $this->full[] = (string)strip_tags($element->children(2));
            $this->eng[] =  (string)strip_tags($element->children(3));

            $content =  $element->children(0)->children(0)->innertext;
            preg_match_all('#<img[^>]+src\s*=\s*(["\'])([^\s"\']+)\\1#im',$content,$src);
            $this->link[] = $src[2][0];

        }


    }

    /**
     *
     */
    function getFull(){

        return $this->full;

    }

    /**
     *
     */
    function getRus(){

        return $this->rus;

    }

    /**
     *
     */
    function getEng(){

        return $this->eng;

    }

    /**
     *
     */
    function getLink(){

        return $this->link;

    }

    /**
     *
     */
    function getCount(){

        return $this->count;

    }

    /**
     * test func
     */
    function getResult(){
        print_r($this->rus);
        print_r($this->full);
        print_r($this->eng);
        print_r($this->link);
    }

}

