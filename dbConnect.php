<?php
/**
 * Created by PhpStorm.
 * User: InnerFlame
 * Date: 08.01.15
 * Time: 16:19
 */
class DbConnect{
    const HOST = "localhost";
    const USER = "root";
    const PASSWORD = "";
    const DB = "parse";

    const TABLE = 'parse_country_tbl';
    const ID = 'id';
    const FULL_NAME = 'full_name';
    const ENG_NAME = 'eng_name';
    const RUS_NAME = 'rus_name';
    const LINK = 'link';

    private $con;

    function __construct() {

        $con = mysqli_connect(self::HOST, self::USER, self::PASSWORD, self::DB);

        if(mysqli_errno($con)){
            echo"sum error";

        }else{
            $this->con = $con;
            $this->createBase();
//            echo"connected ";
        }

    }

    /**
     *
     */
    public function createBase(){

        if(isset($this->con)){

            $sql = "CREATE TABLE parse_country_tbl (
                  id INT(11) NOT NULL AUTO_INCREMENT,
                  full_name VARCHAR(255) DEFAULT NULL,
                  eng_name VARCHAR(255) DEFAULT NULL,
                  rus_name VARCHAR(255) DEFAULT NULL,
                  link VARCHAR(255) DEFAULT NULL,
                  PRIMARY KEY (id)
                )
                ENGINE = INNODB
                AUTO_INCREMENT = 0
                CHARACTER SET utf8
                COLLATE utf8_general_ci;";

            mysqli_query($this->con, $sql);

        }

    }

    /**
     * @param $count
     * @param $eng
     * @param $full
     * @param $rus
     * @param $link
     */
    public function recordData($count, $eng, $full, $rus, $link) {

        # Начинаем формировать sql запрос
        $sql = "INSERT INTO " . self::TABLE . " (`" . self::ID . "`, `" . self::FULL_NAME . "`, `" . self::ENG_NAME . "`, `" . self::RUS_NAME ."`, `" . self::LINK ."`) VALUES";

        # Формируем sql запрос
        for($i=0; $i<$count; $i++){
            $sql .= "( null, '$eng[$i]', '$full[$i]', '$rus[$i]', '$link[$i]'),";
        }
        # Отрезаем лишнюю запятую
        $sql = rtrim( $sql, ',' );

        mysqli_query($this->con, $sql);

    }

}